import java.util.Arrays;

public class POS {
    Object [][] articulos = new Object [][] {};
   
    int [] ventas = new int[] {};
   
    Object [][] productos = new Object [][] {
        {"Tiempo Corto", 5000},
        {"Tiempo Mediano", 10000},
        {"Tiempo Largp", 15000}
    };

    public void Factura() {
        String mensaje = "\t\tFACTURA\n";
        mensaje += "Articulo\t\t\tCantidad\t\t\t\t\tValor unitario\t\t\tValor total\n";
        int total = 0;  
        for (Object [] art: articulos){
            
            int valor_unitario = 0;
            for (Object [] producto: productos){
                if (art[0].equals(producto[0])){
                valor_unitario = (int)(producto[1]);
            }
        }
        int valor_total = valor_unitario*(int)art[1];
        total = total+ valor_total;
        mensaje += art[0] + "\t\t\t" + art[1] + "\t\t\t\t\t" + valor_unitario + "\t\t\t" + valor_total+ "\n";
    }

        mensaje += "TOTAL = " + total;
        System.out.println(mensaje);
        this.nuevaVenta(total);  
    }

    public void ResumenVentas() {
        int total = 0;
        for (int total_ve: ventas){
            total = total+total_ve;
        }

        String mensaje = "El resumen de ventas del dia de hoy es: $" + total;
        System.out.println(mensaje);
    }

    public void nuevoProducto(String art, int cant) {
        articulos = Arrays.copyOf(articulos, articulos.length+1) ;
        articulos[articulos.length-1] = new Object[] {art, cant};
    }

    public void nuevaVenta(int total) {
        ventas = Arrays.copyOf(ventas, ventas.length+1);
        ventas[ventas.length-1] = total;
    }
}